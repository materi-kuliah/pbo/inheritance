/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inheritance;

/**
 *
 * @author mymac
 */
public class Laptop extends Komputer{
    String typeBattery;
    int durasiBattery;


    public String getTypeBattery() {
        return typeBattery;
    }

    public void setTypeBattery(String typeBattery) {
        this.typeBattery = typeBattery;
    }

    public int getDurasiBattery() {
        return durasiBattery;
    }

    public void setDurasiBattery(int durasiBattery) {
        this.durasiBattery = durasiBattery;
    }
    
    
    
}
