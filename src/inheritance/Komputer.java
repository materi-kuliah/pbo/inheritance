/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inheritance;

/**
 *
 * @author mymac
 */
public class Komputer {
    String Processor;
    int Monitor;
    int HardDisk;
    int Memory;

    public String getProcessor() {
        return Processor;
    }

    public void setProcessor(String Processor) {
        this.Processor = Processor;
    }

    public int getMonitor() {
        return Monitor;
    }

    public void setMonitor(int Monitor) {
        this.Monitor = Monitor;
    }

    public int getHardDisk() {
        return HardDisk;
    }

    public void setHardDisk(int HardDisk) {
        this.HardDisk = HardDisk;
    }

    public int getMemory() {
        return Memory;
    }

    public void setMemory(int Memory) {
        this.Memory = Memory;
    }
    
    
}
