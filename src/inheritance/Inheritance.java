/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inheritance;

/**
 *
 * @author mymac
 */
public class Inheritance {

    /**
     * Contoh Penggunaan Kelas Inheritance Pada Java
     * Contoh 1 Single Inheritance
     */
    public static void main(String[] args) {
        Laptop laptop1 = new Laptop();
        laptop1.setTypeBattery("Li Ion");
        laptop1.setDurasiBattery(8);
        
        //method dari induk
        laptop1.setProcessor("Intel");
        laptop1.setMonitor(21);
        laptop1.setHardDisk(200000);
        laptop1.setMemory(512);   
        
        //display Data
        System.out.println(" Data dengan Method ada di Child");
        System.out.println(laptop1.getTypeBattery());
        System.out.println(laptop1.getDurasiBattery());
        
        System.out.println("Data dengan Method ada di Parent");
        System.out.println(laptop1.getHardDisk());
        System.out.println(laptop1.getMemory());
        System.out.println(laptop1.getMonitor());
        System.out.println(laptop1.getProcessor());
        
    
    }
    
}
