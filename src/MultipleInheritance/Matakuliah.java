/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MultipleInheritance;

/**
 *
 * @author mymac
 */
public class Matakuliah extends Dosen{
   String namaMatkul;
   String sks;

    public String getNamaMatkul() {
        return namaMatkul;
    }

    public void setNamaMatkul(String namaMatkul) {
        this.namaMatkul = namaMatkul;
    }

    public String getSks() {
        return sks;
    }

    public void setSks(String sks) {
        this.sks = sks;
    }
   
    public void displayData(){
        //display data
        System.out.println("Data dari Class Matakuliah");
        System.out.println(getNamaMatkul());
        System.out.println(getSks());
        
        System.out.println("Data dari Class Dosen (Parent 2)");
        System.out.println(super.getNIDN());
        
        System.out.println("Data dari Class Karyawan (Parent 1)");
        System.out.println(super.getNIP());
        System.out.println(super.getNama());
        System.out.println(super.getJenisKelamin());
    }
   
}
