/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MultipleInheritance;

/**
 *
 * @author mymac
 */
public class Jurusan extends Dosen{
    String namaJurusan;

    public String getNamaJurusan() {
        return namaJurusan;
    }

    public void setNamaJurusan(String namaJurusan) {
        this.namaJurusan = namaJurusan;
    }
    
    public void displayData(){
        //display data
        System.out.println("\n---------------------------------");
        System.out.println("Data dari Class Jurusan");
        System.out.println(getNamaJurusan());
        
        System.out.println("Data dari Class Dosen (Parent 2)");
        System.out.println(super.getNIDN());
        
        System.out.println("Data dari Class Karyawan (Parent 1)");
        System.out.println(super.getNIP());
        System.out.println(super.getNama());
        System.out.println(super.getJenisKelamin());
    }
}
