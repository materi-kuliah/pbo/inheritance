/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MultipleInheritance;

/**
 *
 * @author mymac
 */
public class MainClass {
    public static void main(String[] args) {
       
        Matakuliah matakuliah = new Matakuliah();
        matakuliah.setNamaMatkul("Pengatar Teknologi Informasi");
        matakuliah.setSks("2");
        
        matakuliah.setNIDN("201902030001");
        
        matakuliah.setNIP("0001");
        matakuliah.setNama("Iwan");
        matakuliah.setJenisKelamin("Laki-laki");
        
        matakuliah.displayData();
        
        Jurusan jurusan = new Jurusan();
        jurusan.setNamaJurusan("Informatika");
        
        jurusan.setNIDN("201902030001");
        
        jurusan.setNIP("0001");
        jurusan.setNama("Iwan");
        jurusan.setJenisKelamin("Laki-laki");
        
        jurusan.displayData();
    }
}
